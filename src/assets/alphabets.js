const en = [
	"a",
	"b",
	"c",
	"d",
	"e",
	"f",
	"g",
	"h",
	"i",
	"j",
	"k",
	"l",
	"m",
	"n",
	"o",
	"p",
	"q",
	"r",
	"s",
	"t",
	"u",
	"v",
	"w",
	"x",
	"y",
	"z",
	" "
];

const ru = [
	"а",
	"б",
	"в",
	"г",
	"д",
	"е",
	"ё",
	"з",
	"ж",
	"и",
	"й",
	"к",
	"л",
	"м",
	"н",
	"о",
	"п",
	"р",
	"с",
	"т",
	"у",
	"ф",
	"х",
	"ц",
	"ч",
	"ш",
	"щ",
	"ъ",
	"ы",
	"ь",
	"э",
	"ю",
	"я",
	" "
];

const ua = [
	"а",
	"б",
	"в",
	"г",
	"ґ",
	"д",
	"е",
	"є",
	"ж",
	"з",
	"и",
	"і",
	"ї",
	"й",
	"к",
	"л",
	"м",
	"н",
	"о",
	"п",
	"р",
	"с",
	"т",
	"у",
	"ф",
	"х",
	"ц",
	"ч",
	"ш",
	"щ",
	"ь",
	"ю",
	"я",
	" "
];

const langs = {
	en,
	ru,
	ua
};

export default langs;

// Съешь еще этих мягких французких булок

// Я волком бы
// выгрыз
// бюрократизм.
// К мандатам
// почтения нету.
// К любым
// чертям с матерями
// катись
// любая бумажка.
// Но эту…
// По длинному фронту
// купе
// и кают
// чиновник
// учтивый движется.
// Сдают паспорта,
// и я
// сдаю
// мою
// пурпурную книжицу.
// К одним паспортам —
// улыбка у рта.
// К другим —
// отношение плевое.
// С почтеньем
// берут, например,
// паспорта
// с двухспальным
// английским левою.
// Глазами
// доброго дядю въев,
// не переставая
// кланяться,
// берут,
// как будто берут чаевые,
// паспорт
// американца.
// На польский —
// глядят,
// как в афишу коза.
// На польский —
// выпяливают глаза
// в тугой
// полицейской слоновости —
// откуда, мол,
// и что это за
// географические новости?

// the five boxing wizards jump quickly

// I won’t beg for your love: it’s laid
// Safely to rest, let the earth settle…
// Don’t expect my jealous letters
// Pouring in to plague your bride.
// But let me, nevertheless, advise you:
// Give her my poems to read in bed,
// Give her my portraits to keep — it’s wise to
// Be kind like that when newly-wed.
// Some say the world will end in fire,
// Some say in ice.
// From what I’ve tasteq of desire
// I hold with those who favor fire.
// But if it had to perish twice,
// I think I know enough of hate
// To say that for destruction ice
// Is also great
// And would suffice.

// Єхидна ґава їжак ще й шиплячі плазуни бігцем форсують
