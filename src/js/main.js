window.onload = function() {
	const executeBtn = document.getElementById("execute-btn");
	const generateKeysBtn = document.getElementById('generate-btn');
	
	const textArea = document.getElementById("text");
	const publicKey = document.getElementById("public");
	const privateKey = document.getElementById('private')

	const action = document.getElementById("action");
	const pError = document.getElementById("p-error");
	const pMessage = document.getElementById("p-message");

	const headers = new Headers({
		'Content-Type': 'application/json',
		'Accept': 'application/json',
	})

	executeBtn.onclick = function(e) {
		e.preventDefault();
		pError.classList.remove("hidden");

		const actionValue = action.options[action.selectedIndex].value.split("_")

		if (!textArea.value) {
			pError.innerHTML = "Error occured. Check your data inputs";
			pError.classList.remove("hidden");
			return;
		}

		let modifiedText;

		if (actionValue[0] === "encrypt") {
			fetch('http://localhost:3000/encrypt', {
				method: 'POST',
				headers,
				body: JSON.stringify({ message: textArea.value })
			})
				.then(res => res.json())
				.then(json => textArea.value = json.data)

		} else if (actionValue[0] === "decrypt") {
			fetch('http://localhost:3000/decrypt', {
				method: 'POST',
				headers,
				body: JSON.stringify({ encrypted: textArea.value })
			})
				.then(res => res.json())
				.then(json => textArea.value = json.data)
		}
	}

	generateKeysBtn.onclick = function(e) {
		e.preventDefault()

		fetch('http://localhost:3000/generate')
			.then(res => res.json())
			.then(json => {
				publicKey.value = json.publicKey
				privateKey.value = json.privateKey
			})
	}


};
