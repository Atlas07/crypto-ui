import langs from "../assets/alphabets.js";

class SymAlgor {
	constructor(alphabet = "en") {
		this.alphabet = langs[alphabet];
	}
	isValid(arr) {
		let flag = true;

		arr.forEach(letter => {
			if (this.alphabet.indexOf(letter) === -1) flag = false;
		});

		return flag;
	}
	getRandomInteger(min, max) {
		let rand = min + Math.random() * (max + 1 - min);
		rand = Math.floor(rand);

		return rand;
	}
}

class Cesar extends SymAlgor {
	constructor(alphabet, key) {
		super(alphabet);
		this.key = +key;
	}

	crypt(str, method) {
		let error;

		const result = str.split("").map(symbol => {
			const letter = this.alphabet.indexOf(symbol.toLowerCase());

			if (letter === -1) {
				error = symbol === " " ? false : true;
				return symbol;
			}

			let temp =
				method === "encrypt" ? letter + this.key : letter - this.key;
			const index = math.mod(temp, this.alphabet.length);

			return this.alphabet[index];
		});

		return { text: result.join(""), error };
	}

	hack(str) {
		let result = "";

		for (let i = 1; i < this.alphabet.length; i++) {
			const res = str.split("").map(symbol => {
				const letter = this.alphabet.indexOf(symbol.toLowerCase());

				if (letter === -1) {
					return symbol;
				}

				const temp = letter - i;
				const index = math.mod(temp, this.alphabet.length);

				return this.alphabet[index];
			});

			result += `shift: ${i}, text: ${res.join("")}\n`;
		}

		return { text: result };
	}
}

let innerMotto = "abcd";
let innerMotoRight;

class Tritemius extends SymAlgor {
	constructor(alphabet) {
		super(alphabet);
	}

	cryptEq(text, koefs, action) {
		const k = this.key(koefs);
		const n = this.alphabet.length;
		const textArr = text.split("");

		if (!this.isValid(textArr)) return { res: text, error: true };

		const result = textArr.map((letter, position) => {
			const x = this.alphabet.indexOf(letter.toLowerCase());
			let newPos;

			if (action === "encrypt-eq") {
				newPos = math.mod(x + k(position), n);
			} else {
				newPos = math.mod(x + n - math.mod(k(position), n), n);
			}

			return this.alphabet[newPos];
		});

		return { res: result.join(""), error: false };
	}

	cryptMot(text, motto, action) {
		// const mottoArr = motto.split("")
		// const mottoLength = mottoArr.length
		const textArr = text.split("");
		const n = this.alphabet.length;

		if (action === "encrypt-mot") {
			let gamma = new Gamma(alphabetValue);

			mottoArr = gamma.spit("");
			innerMotoRight = motto;
		}

		if (action === "decrypt-mot" && motto === innerMotoRight) {
			mottoArr = motto.split("");
		}

		if (!this.isValid(textArr)) {
			return { res: text, error: true };
		}

		const result = textArr.map((letter, position) => {
			let mottoCounter = 0;
			if (mottoCounter > mottoLength) mottoCounter = 0;

			const x = this.alphabet.indexOf(letter.toLowerCase());
			const k = this.alphabet.indexOf(
				mottoArr[mottoCounter].toLowerCase()
			);

			let newPos;

			if (action === "encrypt-mot") {
				newPos = math.mod(x + k, n);
			} else {
				newPos = math.mod(x + n - math.mod(k, n), n);
			}

			mottoCounter++;

			return this.alphabet[newPos];
		});

		console.log(result.join(""));
		return { res: result.join(""), error: false };
	}

	key(koefs) {
		const { a, b, c } = koefs;

		if (+c === 0) {
			return function(p) {
				return +a * p + +b;
			};
		}

		return function(p) {
			return +a * p * p + b * p + +c;
		};
	}
}

class Gamma extends Tritemius {
	constructor(alphabet) {
		super(alphabet);
	}

	generateMotto(length) {
		const arr = new Array(length);

		arr.fill(null);

		const motto = arr.map(symbol => {
			const randomNum = this.getRandomInteger(
				0,
				this.alphabet.length - 1
			);

			return this.alphabet[randomNum];
		});

		return motto.join("");
	}

	encryptXOR(text, motto) {
		const mottoArr = motto.split("");
		const mottoLength = mottoArr.length;
		const textArr = text.split("");

		if (!this.isValid(textArr) || !this.isValid(mottoArr)) {
			return { res: text, error: true };
		}

		let mottoCounter = 0;

		const result = textArr.map((letter, position) => {
			if (mottoCounter > mottoLength) mottoCounter = 0;

			const x = this.alphabet.indexOf(letter);
			const k = this.alphabet.indexOf(mottoArr[mottoCounter]);
			let newPos = x ^ k;
			mottoCounter++;

			// console.log(
			// 	x.toString(16),
			// 	(x ^ k).toString(16),
			// 	(x ^ k ^ k).toString(16)
			// )

			return newPos.toString(16);
		});

		for (let i = 0; i < result.length; i++) {
			if (i % 2 !== 0) {
				result.splice(i, 0, "-");
			}
		}

		return { res: result.join(""), error: false };
	}

	decryptXOR(text, motto) {
		const mottoArr = motto.split("");
		const mottoLength = mottoArr.length;
		const textArr = text.split("-");

		if (!this.isValid(mottoArr)) {
			return { res: text, error: true };
		}

		let mottoCounter = 0;

		const result = textArr.map((letter, position) => {
			letter = parseInt(letter, 16);

			if (mottoCounter > mottoLength) mottoCounter = 0;

			const k = this.alphabet.indexOf(mottoArr[mottoCounter]);
			// let newPos = +letter.toString(16) ^ +k.toString(16)

			// console.log(
			// 	letter,
			// 	typeof letter,
			// 	parseInt(letter, 16),
			// 	typeof parseInt(letter, 16),
			// 	letter.toString(16),
			// 	typeof letter.toString(16)
			// )

			const newPos = +(letter ^ k).toString(16);
			console.log(letter, k, newPos);
			mottoCounter++;

			// console.log("decrypt")

			return this.alphabet[newPos];
		});

		return { res: result.join(""), error: false };
	}
}

class Book extends SymAlgor {
	constructor(text) {
		super();

		// text = `some\ntext\nishe\nreha`;

		const tempText = text.split(/\r\n|\r|\n/g);
		const bookArr = tempText.map(line => {
			const lineArr = line.split("");
			return lineArr;
		});

		this.book = bookArr;
		console.log(bookArr);
	}

	transformId(id, length = 2) {
		return ("00" + id).slice(-length);
	}

	encrypt(text) {
		let resStr = "";

		const res = text.split("").map(letter => {
			for (let i = 0; i < this.book.length; i++) {
				for (let j = 0; j < this.book.length; j++) {
					if (this.book[i][j] === letter) {
						resStr += `${this.transformId(i)}-${this.transformId(
							j
						)},`;
						return { i, j };
					}
				}
			}
		});

		resStr = resStr.slice(0, -1);

		return { res: resStr };
	}

	decrypt(text) {
		const textArr = text.split(",");

		const res = textArr.map(letter => {
			const temp = letter.split("-");
			const data = {
				x: parseInt(temp[0], 10),
				y: parseInt(temp[1], 10)
			};

			console.log(this.book[data.x], this.book[data.y]);

			return this.book[data.x][data.y];
		});

		return { res: res.join("") };
	}
}

export { Cesar, Tritemius, Gamma, Book };
